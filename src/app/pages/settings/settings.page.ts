import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
const project = require('package.json');

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  constructor(
    private apollo: Apollo,
    public navCtrl: NavController,
    private router: Router,
    private storage: StorageService
  ) {}

  get version() {
    return `${project.version}`;
  }

  ngOnInit() {}
}
